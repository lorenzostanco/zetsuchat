<?php
	
	session_start();

	// Requested action
	switch ($_REQUEST['action']) {
		
		// Read messages
		case 'refresh':
			
			// Last message ID
			$last = intval($_REQUEST['last']);
			
			// Create random messages
			$words = array('hello', 'world', 'zetsu', 'chat', 'test', 'message', 'lorem', 'ipsum', 'dolor', 'sit', 'amet');
			$users = array('john', 'jack', 'paul');
			$messages = array();
			$messagesCount = rand(1, 3);
			for ($i = 0; $i < $messagesCount; $i++) {
				$userId = rand(1, 3);
				$messages[] = array(
					'id' => $last + $i + 1, 'userId' => $userId, 'username' => $users[$userId - 1], 
					'text' => $words[array_rand($words)] . ' ' . $words[array_rand($words)] . ' ' . $words[array_rand($words)],
					'timestamp' => time() - $messagesCount + $i
				);
			}
			
			// Append last message sent, if present
			if (!empty($_SESSION['last_message'])) {
				$_SESSION['last_message']['id'] = $last + $messagesCount + 1;
				$_SESSION['last_message']['timestamp'] = time();
				$messages[] = $_SESSION['last_message'];
				unset($_SESSION['last_message']);
			}
			
			// Output JSON
			echo json_encode($messages);
			exit();
		
		// Send message
		case 'send':
			
			// Save message
			$timestamp = intval($_REQUEST['timestamp']);
			$_SESSION['last_message'] = array(
				'id' => null, 
				'userId' => $_REQUEST['userId'], 
				'username' => $_REQUEST['username'], 
				'text' => $_REQUEST['text'],
				'timestamp' => ($timestamp ? $timestamp : time())
			);
			
			// Output JSON
			echo json_encode(array('error' => '0'));
			exit();
			
	}
	
?>