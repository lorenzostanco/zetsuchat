ZetsuChat
=========

A Mootools minimal tiny chat widget for general purpose website integration.

ZetsuChatWidget
---------------

Manages a *ZetsuChat* via DOM elements.

	new ZetsuChatWidget(chat, historyWrapper, messageInput, sendButton, options);

 - *chat*: A *ZetsuChat* object.
 - *historyWrapper*: The DOM element that has to contain chat messages.
 - *messageInput*: Message input box (DOM input or textarea)
 - *sendButton*: Send button, optional (DOM element)

**Options:**

 - *refreshInterval*: In milliseconds, default: 5000.
 - *autoScroll*: Chat scroll to bottom when new messages arrive, default TRUE.
 - *createMessage*: A "render" function that returns a DOM element from a *ZetsuChatMessage* passed as first argument.
 
**Methods:**
 
 - *start()*: Start refreshing messages.
 - *stop()*: Stop refreshing messages.
 - *send()*: Sends the filled-in message.
 - *appendMessages(messages)*: Appends messages to the history wrapper.
 - *setEnabled(enable)*: Enable/disable all input DOM elements waiting for message sending.
 - *createMessageElement(message)*: Create a message element from a ZetsuChatMessage. Used internally. Override to build custom DOM elements for messages.
 
**Events:**

 - *sent*: Message sent.
 - *enabled*: Widget has been enabled.
 - *disabled*: Widget has been disabled.
 
ZetsuChat
---------

Represent a chat, provides methods for sending messages and refreshing message history.

	new ZetsuChat(userId, username, options);

 - *userId*: User identificator.
 - *username*: User name.
    
**Options:**

 - *messageHistory*: Message history limit, default 100.
 - *scriptUrl*: URL for AJAX, default "chat.php".
 - *requestMethod*: Method for AJAX, default "post".
 - *refreshAfterSent*: Auto-refresh messages history after a message has been sent, default TRUE.
 
**Methods:**

 - *send(text)*: Sends a message, only text message is given.
 - *refresh()*: Refresh message history.
 - *getMessages()*: Return the whole message history as array of *ZetsuChatMessage* objects.

**Events:**

 - *sending*: Trying to send a message.
 - *sent(message)*: Message sent.
 - *refreshing*: Trying to refresh message history.
 - *refreshed(newMessages)*: Message history refreshed, new messages loaded.
 - *error(message)*: Error.
 
ZetsuChatMessage
----------------

Represent a chat message.

	new ZetsuChatMessage(id, userId, username, text, timestamp);
	new ZetsuChatMessage(data); // Import data from a generic object
    
 - *id*: Message unique id, can be null for new messages ready to be sent.
 - *userId*: User identificator.
 - *username*: User name.
 - *text*: The message.
 - *timestamp*: Date/time as Unix timestamp, if not given is setted to current date/time.
    
**Methods:**

 - *getDateTime(format)*: Get date/time for message, format is used in Mootools' *Date.format()*, default "%d/%m/%Y %H:%M".
 - *getDate(format)*: Get date for message, format is used in Mootools' *Date.format()*, default "%d/%m/%Y".
 - *getTime(format)*: Get time for message, format is used in Mootools' *Date.format()*, default "%H:%M".
 - *getSmartDateTime()*: Returns *getTime()* for today messages, *getDateTime()* for older messages.