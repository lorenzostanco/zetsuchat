/*
---
description: A minimal tiny chat widget for general purpose website integration. 

license: MIT-style

authors:
- Lorenzo Stanco

requires:
- core/1.2.5: '*'
- more/1.2.5: Date

provides: [ZetsuChatMessage, ZetsuChat, ZetsuChatWidget]

...
*/

/**
 * Represent a chat message
 */
var ZetsuChatMessage = new Class({
	
	id: null,
	userId: null,
	username: null,
	text: null,
	timestamp: null,
	
	initialize: function(id, userId, username, text, timestamp) {
	
		// One parameter? Clone
		if (!userId && !username && !text) {
			userId = id.userId;
			username = id.username;
			text = id.text;
			if (id.timestamp) timestamp = id.timestamp;
			id = id.id;
		}
	
		// Setup
		this.id = id;
		this.userId = userId,
		this.username = username,
		this.text = text,
		this.timestamp = (timestamp ? timestamp : (new Date().getTime() / 1000).toInt());
		
		return this;
		
	},
	
	/**
	 * Get message time as string
	 * @param format Format for Mootools' Date.format(), default '%d/%m/%Y %H:%M'
	 * @returns Date/time formatted as string
	 */
	getDateTime: function(format) {
		if (!format) format = '%d/%m/%Y %H:%M';
		var date = new Date();
		date.setTime(this.timestamp * 1000);
		return date.format(format);
	},

	/**
	 * Get message date as string
	 * @param format Format for Mootools' Date.format(), default '%d/%m/%Y'
	 * @returns Date formatted as string
	 */
	getDate: function(format) {
		if (!format) format = '%d/%m/%Y';
		return this.getDateTime(format);
	},

	/**
	 * Get message time as string
	 * @param format Format for Mootools' Date.format(), default '%H:%M'
	 * @returns Time formatted as string
	 */
	getTime: function(format) {
		if (!format) format = '%H:%M';
		return this.getDateTime(format);
	},
	
	/**
	 * Returns getTime() for today messages, getDateTime() for older messages.
	 * @returns Date/time formatted as string
	 */
	getSmartDateTime: function() {
		var today = (new Date().format('%d/%m/%Y') == this.getDate('%d/%m/%Y'));
		if (today) return this.getTime();
		else return this.getDateTime();
	}
	
});

/**
 * Represent a chat, provides methods for sending messages and refreshing message history.
 */
var ZetsuChat = new Class({
	
	Implements: [Options, Events],
	
	/* Options */
	options: {
		messageHistory: 100,
		scriptUrl: 'chat.php',
		requestMethod: 'post',
		refreshAfterSent: true
	},
	
	/* Logged user info */
	userId: 0,
	username: '',
	
	/* Messages history */
	messages: [],
	messageLastId: 0,
	messageLastSent: null,
	
	/* Ajax requests */
	requestRefresh: null,
	requestSend: null,
	
	initialize: function(userId, username, options) {
		
		// Setup
		this.setOptions(options);
		this.userId = userId;
		this.username = username;

		// Setup refresh request
		this.requestRefresh = new Request.JSON({
			url: this.options.scriptUrl,
			method: this.options.requestMethod,
			link: 'ignore',
			onSuccess: function(json) {
				if (json.error && json.error != '0') this.fireEvent('error', ['Cannot load messages']);
				else {
					var messages = [];
					json.each(function(j) { 
						messages.push(new ZetsuChatMessage(j)); 
						this.messageLastId = j.id; }.bind(this
					));
					// TODO: if (messages.lenght > this.options.messageHistory) clear old messages
					this.fireEvent('refreshed', [ messages ]);
				}
			}.bind(this),
			onError: function() {
				this.fireEvent('error', ['Cannot load messages']);
			}.bind(this)
		});

		// Setup send request
		this.requestSend = new Request.JSON({
			url: this.options.scriptUrl,
			method: this.options.requestMethod,
			link: 'chain',
			onSuccess: function(json) {
				if (json.error && json.error != '0') this.fireEvent('error', ['Cannot load messages']);
				else {
					this.fireEvent('sent', [ this.messageLastSent ]);
					if (this.options.refreshAfterSent) this.refresh();
				}
			}.bind(this),
			onError: function() {
				this.fireEvent('error', ['Cannot load messages']);
			}.bind(this)
		});
		
		return this;
		
	},
	
	/**
	 * Send a message, fires 'sending', 'sent' and 'error'
	 * @param message Message to be sent
	 * @returns This
	 */
	send: function(text) {
		this.fireEvent('sending');
		this.messageLastSent = new ZetsuChatMessage(null, this.userId, this.username, text);
		this.requestRefresh.cancel(); // Cancel refreshing request, if any
		
		// Send message
		this.requestSend.send({ data: { 
			'userId': this.messageLastSent.userId,
			'username': this.messageLastSent.username,
			'text': this.messageLastSent.text,
			'timestamp': this.messageLastSent.timestamp,
			'action': 'send' 
		} });
		
		return this;
		
	},

	/**
	 * Refresh message history, fires 'refreshing', 'refreshed' and 'error'
	 * @returns This
	 */
	refresh: function() {
		this.fireEvent('refreshing');
		this.requestRefresh.send({ data: { 'action': 'refresh', 'last': this.messageLastId }});
		return this;
	},
	
	/**
	 * Get the whole message history.
	 * @returns An array of ZetsuChatMessage objects
	 */
	getMessages: function() {
		return this.messages;
	}
	
});

/**
 * Manages a ZetsuChat via DOM elements
 */
var ZetsuChatWidget = new Class({

	Implements: [Options, Events],
	
	/* Options */
	options: {
		refreshInterval: 5000,
		autoScroll: true,
		createMessage: function(message) {
			var element = new Element('p');
			element.set('text', ' ' + message.text);
			new Element('span').addClass('username').set('text', message.username + ':').inject(element, 'top');
			element.set('html', ' ' + element.get('html'));
			new Element('span').addClass('date').set('text', message.getSmartDateTime()).inject(element, 'top');
			return element;
		}
	},
	
	chat: null,
	historyWrapper: null,
	messageInput: null,
	sendButton: null,
	
	interval: null,
	
	/**
	 * Chat widget constructor.
	 * @param chat ZetsuChat object
	 * @param userId Current user identificator
	 * @param username Current user name
	 * @param historyWrapper History wrapper (DOM element)
	 * @param messageInput Message input box (DOM input or textarea)
	 * @param sendButton Send button, optional (DOM element)
	 * @param options Class options
	 * @returns This
	 */
	initialize: function(chat, historyWrapper, messageInput, sendButton, options) {
		
		// Setup
		this.setOptions(options);
		this.chat = chat;
		this.historyWrapper = document.id(historyWrapper);
		this.messageInput = document.id(messageInput);
		this.sendButton = (sendButton ? document.id(sendButton) : null);
		
		// Map chat events
		this.chat.addEvents({
			'sending'  : this.setEnabled.pass(false, this),
			'refreshed': this.appendMessages.bind(this),
			'error'    : this.setEnabled.pass(false, this),
			'sent'     : function() { 
				this.fireEvent('sent');
				this.setEnabled(true); 
				if (this.options.autoScroll) this.scrollToBottomFlag = true;
			}.bind(this)
		});
		
		// User interactions
		if (this.sendButton) this.sendButton.addEvent('click', this.send.bind(this));
		this.messageInput.addEvent('keydown', function(e) { 
			if (e.key != 'enter') return true;
			this.send(); 
			e.stop();
			return false;
		}.bind(this));
		
		// Go
		this.setEnabled(true);
		if (this.options.autoScroll) this.scrollToBottomFlag = true;
		this.start();
		
		return this;
		
	},
	
	/**
	 * Start refreshing messages.
	 * @returns This
	 */
	start: function() {
		if (!this.interval) {
			this.interval = this.chat.refresh.periodical(this.options.refreshInterval, this.chat);
			this.chat.refresh(); }
		return this;
	},

	/**
	 * Stop refreshing messages.
	 * @returns This
	 */
	stop: function() {
		if (this.interval) clearInterval(this.interval);
		this.interval = null;
		return this;
	},
	
	/**
	 * Sends the filled-in message.
	 * @returns This
	 */
	send: function() {
		var text = this.messageInput.get('value').trim();
		if (text != '') this.chat.send(text);
		return this;
	},

	/**
	 * Appends messages to the history wrapper.
	 * @returns This
	 */
	appendMessages: function(messages) {
		
		// See if I have to scroll to bottom after message injection
		if (this.options.autoScroll && !this.scrollToBottomFlag) {
			var historyWrapperScrollPosition = this.historyWrapper.getScrollSize().y - this.historyWrapper.getSize().y;
			this.scrollToBottomFlag = (this.historyWrapper.getScroll().y == historyWrapperScrollPosition);
		}

		// Inject messages
		messages.each(function(message) {
			this.createMessageElement(message).inject(this.historyWrapper);
		}.bind(this));
		
		// Scroll if necessary
		if (this.options.autoScroll && this.scrollToBottomFlag) this.scrollToBottom();
		
		return this;
		
	},
	
	/**
	 * Scroll the message history to bottom
	 * @returns This
	 */
	scrollToBottom: function() {
		this.historyWrapper.scrollTo(0, this.historyWrapper.getScrollSize().y);
		this.scrollToBottomFlag = false;
	},
	
	/**
	 * Enable/disable all input DOM elements waiting for message sending.
	 * @returns This
	 */
	setEnabled: function(enable) {
		if (enable) this.start(); else this.stop();
		this.messageInput.addClass('sending');
		this.messageInput.set('disabled', !enable);
		if (enable) this.messageInput.set('value', '');
		if (this.sendButton) {
			if (enable) this.sendButton.removeClass('sending');
			else this.sendButton.addClass('sending');
			this.sendButton.set('disabled', !enable); }
		this.fireEvent(enable ? 'enabled' : 'disabled');
		return this;
	},
	
	/**
	 * Create a message element from a ZetsuChatMessage.
	 * Used internally. It calls corresponding method from options.
	 * @param message
	 * @returns Element
	 */
	createMessageElement: function(message) {
		var element = this.options.createMessage(message);
		return element;
	}
	
});
